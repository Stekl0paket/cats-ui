import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';
/**
 * Класс для реализации логики со страницы рейтинга в приложении котиков.
 *
 */
export class RatingPage {
  private page: Page;
  public likesSelector: string


  constructor({
    page,
  }: {
    page: Page;
  }) {
    this.page = page;
    this.likesSelector = "//table[1]/tbody/*/td[3]"

  }


  async openRatingPage() {
    return await test.step('Открываю страницу с рейтингом котиков', async () => {
      await this.page.goto('/rating')
    })
  }


}

export type RatingPageFixture = TestFixture<
  RatingPage,
  {
    page: Page;
  }
>;

export const ratingPageFixture: RatingPageFixture = async (
  { page },
  use
) => {
  const ratingPage = new RatingPage({ page });

  await use(ratingPage);
};

