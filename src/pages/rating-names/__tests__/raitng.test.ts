import { test as base, expect } from '@playwright/test';
import { RatingPage, ratingPageFixture } from '../__page-object__';
import { errorResponse } from '../__mocks__/error-response';
import { reverse } from 'dns';

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({ page, ratingPage   }) => {
  /**
   * Замокать ответ метода получения рейтинга ошибкой на стороне сервера
   * Перейти на страницу рейтинга
   * Проверить, что отображается текст ошибка загрузки рейтинга
   */
  await page.route(
    request => request.href.includes('api/likes/cats/rating'),
    async route => {
      await route.fulfill({
        // headers: {
        //   "Access-Control-Allow-Origin": "*",
        //   "Content-Length": "1092",
        //   "Content-Type": "application/json; charset=utf-8",
        //   "Date": "Sun, 19 May 2024 22:11:04 GMT",
          
        //   "Etag": 'W/"444-Kj5dfKhJIRf5fE0FczyCjfTlkLI"',
        //   "Request-Id": "ceb3e138425bddac587d0f3a8ebcb660",
        //   "Server": "nginx",
        //   "X-Powered-By": "Express"
        // },
        json: errorResponse,
        status: 500
        
        
        
        
      });
    }
  );
  await ratingPage.openRatingPage()
  await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible();

});

test.skip('Рейтинг котиков отображается', async ({ page, ratingPage }) => {
  /**
   * Перейти на страницу рейтинга
   * Проверить, что рейтинг количества лайков отображается по убыванию
   */
  await ratingPage.openRatingPage()
  const likes = (await page.locator(ratingPage.likesSelector).allInnerTexts()).map(Number)

  const sortedLikes = [...likes].sort((a, b) => b - a)
  console.log(likes, sortedLikes)
  await expect(likes.every((like, index) => like === sortedLikes[index])).toBe(true)
}); 
